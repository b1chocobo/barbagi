<?php
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  $USERNAME=$_SESSION['user'];
  include "db_connection.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barbagi</title>
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <link rel="stylesheet" type="text/css" href="styles/photo.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      $PHOTO=$_GET['photo'];
      $query="SELECT * FROM photo WHERE id_photo='$PHOTO'";
      $statement=$conn->query($query);
      $statement->setFetchMode(PDO::FETCH_ASSOC);
      if ($result=$statement->fetch())
      {
        $GALLERY=$result['id_gallery'];
        $query="SELECT * FROM gallery WHERE id_gallery='$GALLERY'";
        $statement=$conn->query($query);
        $result2=$statement->fetch();
        if ($result2['isprivate']==0||$result2['username']==$USERNAME)
        {
          ?>
            <img id="photo" src="<?php echo 'upload/'.$result['id_photo'].'.'.$result['file_type']?>">
          <?php
        }
        else
        {
          echo "you're not authorized to see the photo";
        }
      }
      else
      {
        echo "the photo you're trying to access doesn't exist";
      }
    ?>
  </body>
</html>
