<?php
  session_start();
  if (isset($_SESSION['user']))
  {
    header('Location: home.php');
  }
?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta property="og:title" content="Barbagi:A Professional Photo Sharing Site">
    <meta property="og:description" content="Barbagi is a place where professional photographers can share their photos.">
    <link rel="stylesheet" type="text/css" href="styles/index.css">
    <title>Barbagi</title>
  </head>
  <body>
    <br><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"><br>
    <svg viewBox="0 0 800 600" id="anim">
      <symbol id="s-text">
        <text text-anchor="middle" x="50%" y="10%" class="text--line2">BARBAGI</text>
      </symbol>
      <g class="g-ants">
        <use xlink:href="#s-text" class="text-copy"></use>
        <use xlink:href="#s-text" class="text-copy"></use>
        <use xlink:href="#s-text" class="text-copy"></use>
        <use xlink:href="#s-text" class="text-copy"></use>
        <use xlink:href="#s-text" class="text-copy"></use>
      </g>
    </svg>
      <form name="LOGIN" method="post" action="login_auth.php">
        <div class="loginBox">
          <input id="inpUsernameLogin" type="text" name="username" placeholder="Username"/>
          <input id="inpPasswordLogin" type="password" name="password" placeholder="Password"/>
          <input id="btnLogin" class="button" type="submit" value="Login"/>
          <p>Don't have an account? <a href="signup.php" id="signUp">Sign Up</a></p>
        </div>
      </form>
    <?php
      if(isset($_SESSION['empty']))
  	  {
    	  unset($_SESSION['empty']);
    	  echo "<script>alert('Please enter your username and password')</script>";
    	}
    	if(isset($_SESSION['wrong']))
    	{
    	  unset($_SESSION['wrong']);
    	  echo"<script>alert('Either the username or the password is incorrect. Please check if your Caps Lock is on')</script>";
    	}
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="scripts/index.js"></script>
  </body>
</html>
