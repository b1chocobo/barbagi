<?php
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  $USERNAME=$_SESSION['user'];
  include "db_connection.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barbagi</title>
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <link rel="stylesheet" type="text/css" href="styles/gallery.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      $GALLERY=$_GET['gallery'];
      $query="SELECT * FROM gallery WHERE id_gallery='$GALLERY'";
      $statement=$conn->query($query);
      $statement->setFetchMode(PDO::FETCH_ASSOC);
      $result=$statement->fetch();
      if (!$result)
      {
        echo "the gallery you're trying to access doesn't exist";
      }
      else if ($result['isprivate'] && $result['username']!=$USERNAME)
      {
        echo "you're not authorized to open the gallery";
      }
      else
      {
        $query="SELECT * FROM photo WHERE id_gallery='$GALLERY' ORDER BY time_upload DESC";
        $statement=$conn->query($query);
        if ($statement->rowCount()==0)
        {
          echo $result['nama_gallery']." has no picture.";
        }
        else
        {
          while ($row=$statement->fetch())
          {
            echo "<a href='photo.php?photo=".$row['id_photo']."'><img src='upload/".$row['id_photo'].".".$row['file_type']."' alt='".$row['title']."'></a>";
          }
        }
      }
    ?>
  </body>
</html>
