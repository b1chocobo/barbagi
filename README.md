# BARBAGI #

This is our team project for Web Programming Class at Duta Wacana Christian University. It is intended to be a social media where people can share their photos. You can access it [here](https://vhost.ti.ukdw.ac.id/~gn16b1/)

### How do I get set up? ###

* This project is built using [Atom](https://atom.io/) for text editing, [XAMPP](https://www.apachefriends.org/index.html) for PHP and Database testing, and [Chrome](https://www.google.com/chrome/) as web browser.
* In order to get the PHP and Database running, you need to install XAMPP, copy all the files into ./xampp/htdocs, run xampp-control.exe, start Apache and MySQL, and then access localhost from your browser.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Kevin Valiant - kevin.valiant@ti.ukdw.ac.id

### License ###
