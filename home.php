<?php
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  $USERNAME=$_SESSION['user'];
  include "db_connection.php";

?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <title>Barbagi:Home</title>
    <link rel="stylesheet" type="text/css" href="styles/home.css">
    <link rel="stylesheet" type="text/css" href="styles/header.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu" id="active">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      $follow = array();
      $query="SELECT * FROM follow WHERE username='$USERNAME'";
      $statement=$conn->query($query);
      $statement->setFetchMode(PDO::FETCH_ASSOC);
      while ($row=$statement->fetch())
      {
        array_push($follow,$row['user_followed']);
      }
      $query="SELECT * FROM home";
      $statement=$conn->query($query);
      while ($row=$statement->fetch())
      {
        if (in_array($row['username'],$follow))
        {
          ?>
      <div class="photo">
        <table width="100%">
          <tr>
            <td>
              <a href="photo.php?photo=<?php echo $row['id_photo'] ?>"><img src="<?php echo 'upload/'.$row['id_photo'].'.'.$row['file_type'] ?>"></a>
            </td>
            <td>
              <h2><?php echo $row['title'];  ?></h2>
              <h4>by <a href="profile.php?profile=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a></h4>
            </td>
          </tr>
        </table>
      </div>
          <?php
        }
      }
    ?>
  </body>
</html>
