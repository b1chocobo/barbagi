<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="styles/myprofile.css">
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <meta charset="utf-8">
    <title>
      Barbagi:My Profile
    </title>
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu" id="active">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      session_start();
      $USERNAME=$_SESSION['user'];
      include "db_connection.php";
      $query="SELECT * FROM user WHERE username='$USERNAME'";
      $statement=$conn->query($query);
      $statement->setFetchMode(PDO::FETCH_ASSOC);
      $result=$statement->fetch();
    ?>
    <table>
      <tbody>
        <tr>
          <td>
            <img src="<?php echo $result['profile_picture'] ?>" width=150px height=150px alt='profile picture'>
          </td>
          <td>
            <span id="nama"><?php echo $result['nama']?></span>
            <br></br>
            <p id="desc"><?php echo $result['description'] ?></p>
            <a href="editprofile.php" id=editProfile>Edit Profile</a>
          </td>
        </tr>
      <tbody>
    <table>
    <div id="gallery">
    <?php
      $MAIN=$result['main_gallery'];
      $query="SELECT * FROM photo WHERE id_gallery='$MAIN' ORDER BY time_upload DESC LIMIT 1";
      $statement=$conn->query($query);
      if ($result=$statement->fetch())
      {
        echo "<a href='gallery.php?gallery=".$MAIN."'><img src='upload/".$result['id_photo'].".".$result['file_type']."' alt='Main Gallery'></a>";
      }
      else
      {
        echo "<p id='nopic'>Main Gallery has no picture.</p>";
      }
      $query="SELECT * FROM gallery WHERE id_gallery<>'$MAIN' AND username='$USERNAME' ORDER BY dateUpdated";
      $statement=$conn->query($query);
      while ($row=$statement->fetch())
      {
        $GALLERY=$row['id_gallery'];
        $query2="SELECT * FROM photo WHERE id_gallery='$GALLERY' ORDER BY time_upload DESC LIMIT 1";
        $statement2=$conn->query($query2);
        if ($result=$statement2->fetch())
        {
          echo "<a href='gallery.php?gallery=".$GALLERY."'><img src='upload/".$result['id_photo'].".".$result['file_type']."' alt='".$row['nama_gallery']."'></a>";
        }
        else {
          echo "<p id='nopic'>".$row['nama_gallery']." has no picture.</p>";
        }
      }
    ?>
    </div>
  </body>
</html>
