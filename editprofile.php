<?php
  session_start();
  $USERNAME=$_SESSION['user'];
  include "db_connection.php";
  $query="SELECT * FROM user WHERE username='$USERNAME'";
  $statement=$conn->query($query);
  $statement->setFetchMode(PDO::FETCH_ASSOC);
  $result=$statement->fetch();
  if (isset($_POST['chpic']))
  {
    if (is_uploaded_file($_FILES['newpic']['tmp_name']))
    {
      $ext=pathinfo($_FILES['newpic']['name'],PATHINFO_EXTENSION);
      $loc="upload/".$USERNAME.".".$ext;
      if (move_uploaded_file($_FILES['newpic']['tmp_name'],$loc))
      {
        $query="UPDATE user SET profile_picture='upload/".$USERNAME.".".$ext."' WHERE username='$USERNAME'";
        $statement=$conn->prepare($query);
        $statement->execute();
      }
    }
  }
  else if(isset($_POST['chinfo']))
  {
    $NAMA=$_POST['nama'];
    $DESCRIPTION=$_POST['description'];
    $query="UPDATE user SET nama='$NAMA',description='$DESCRIPTION' WHERE username='$USERNAME'";
    $statement=$conn->prepare($query);
    $statement->execute();
  }
  else if(isset($_POST['chpass']))
  {
    $OLD_PASSWORD=$_POST['oldpassword'];
    $NEW_PASSWORD=$_POST['newpassword'];
    if ($OLD_PASSWORD!=$result['password'])
    {
      $_SESSION['wrong']='wrong';
    }
    else
    {
      $query="UPDATE user SET password='$NEW_PASSWORD' WHERE username='$USERNAME'";
      $statement=$conn->prepare($query);
      $statement->execute();
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barbagi</title>
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <link rel="stylesheet" type="text/css" href="styles/editprofile.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      $query="SELECT * FROM user WHERE username='$USERNAME'";
      $statement=$conn->query($query);
      $result=$statement->fetch();
      $PROFILE=$result['profile_picture'];
      echo "<img id='profpic' src='".$PROFILE."' alt='user profile'>";
    ?>
    <div id="changepic">
      <form method='post' enctype='multipart/form-data' action='editprofile.php'>
        <input type='file' name='newpic' accept='image/*'>
        <input type="submit" value="Change Avatar" name="chpic">
      </form>
    </div>
    <form method="post" action="editprofile.php">
    <?php
      $NAMA=$result['nama'];
      $DESCRIPTION=$result['description'];
      echo "<input type='text' name='nama' placeholder='Name' value='$NAMA'/>";
      echo "<input type='text' name='description' placeholder='Description' value='$DESCRIPTION'>";
    ?>
      <input type="submit" name="chinfo" value="Save Changes"/>
    </form>
    <form method="post" action="editprofile.php">
      <input type="password" name="oldpassword" placeholder="Old Password"/>
      <input type="password" name="newpassword" placeholder="New Password"/>
      <input type="password" placeholder="Confirm New Password"/>
      <input type="submit" name="chpass" value="Save Password"/>
    </form>
  </body>
</html>
