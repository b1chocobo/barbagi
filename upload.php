<?php
  include "db_connection.php";
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  $USERNAME=$_SESSION['user'];
  if (isset($_SESSION['success']))
  {
    unset($_SESSION['success']);
    echo "<script>alert('Upload image success')</script>";
  }
  else if (isset($_POST['gallerycreate']))
  {
    $NAME=$_POST['galleryname'];
    $DESCRIPTION=$_POST['description'];
    $PRIVATE=$_POST['private'];
    $query="SELECT * FROM gallery WHERE username='$USERNAME' AND nama_gallery='$NAME'";
    $statement=$conn->query($query);
    $statement->setFetchMode(PDO::FETCH_ASSOC);
    $result=$statement->fetch();
    if ($result)
    {
      $_SESSION['exist']='exist';
    }
    else
    {
      $query="SELECT * FROM keystore";
      $statement=$conn->query($query);
      $result=$statement->fetch();
      $next_gallery_id=$result['next_gallery_id'];
      $query="INSERT INTO gallery(username,id_gallery,nama_gallery,description,isprivate) VALUES ('$USERNAME','$next_gallery_id','$NAME','$DESCRIPTION','$PRIVATE')";
      $statement=$conn->prepare($query);
      $statement->execute();
      $next_gallery_id=$next_gallery_id+1;
      $query="UPDATE keystore SET next_gallery_id='$next_gallery_id'";
      $statement=$conn->prepare($query);
      $statement->execute();
    }
  }
?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <title>Barbagi:Upload</title>
    <link rel="stylesheet" type="text/css" href="styles/upload.css">
    <link rel="stylesheet" type="text/css" href="styles/header.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu" id="active">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <table width=100% height=100% id="ch">
      <tbody>
        <tr>
          <td><h2>Upload New Photo</h2></td>
          <td><h2>Create New Gallery</h2></td>
        </tr>
        <tr>
          <td>
            <form method="post" enctype="multipart/form-data" action="do_upload.php" id="diUpload">
              <label id="file-input"><input type="file" name="imgfile" accept="image/*"></label>
              <input type="text" name="imgtitle" placeholder="Title">
              <input type="text" name="imgcap" placeholder="Caption">
              <p>Album: <select id="dropdown" name="gallery">
              <?php
                $query="SELECT * FROM gallery WHERE username='$USERNAME'";
                $statement=$conn->query($query);
                $statement->setFetchMode(PDO::FETCH_ASSOC);
                while ($row=$statement->fetch())
                {
                  echo "<option value='".$row['id_gallery']."'>".$row['nama_gallery']."</option>";
                }
              ?>
              </select>
              </p>
              <input type="submit" value="Upload Image" name="imgsubmit" class="button">
            </form>
          </td>
          <td>
            <form method="post" action="upload.php">
              <input type="text" name="galleryname" placeholder="Gallery Name">
              <input type="text" name="description" placeholder="Description">
              <p>Private: <input type="radio" name="private" value="1">On<input type="radio" name="private" value="0">Off</p>
              <input type="submit" name="gallerycreate" value="Create Gallery" class="button">
            </form>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
