<?php
  include "db_connection.php";
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  if (!isset($_GET['profile'])||$_GET['profile']==$_SESSION['user'])
  {
    header('Location: myprofile.php');
  }
  $PROFILE=$_GET['profile'];
  $USERNAME=$_SESSION['user'];
  if (isset($_POST['follow']))
  {
    $query="INSERT INTO follow(username,user_followed) VALUES ('$USERNAME','$PROFILE')";
    $statement=$conn->prepare($query);
    $statement->execute();
  }
  else if (isset($_POST['unfollow']))
  {
    $query="DELETE FROM follow WHERE username='$USERNAME' AND user_followed='$PROFILE'";
    $statement=$conn->prepare($query);
    $statement->execute();
  }
  $query="SELECT * FROM user WHERE username='$PROFILE'";
  $statement=$conn->query($query);
  $statement->setFetchMode(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barbagi</title>
    <link rel="stylesheet" type="text/css" href="styles/header.css">
    <link rel="stylesheet" type="text/css" href="styles/profile.css">
  </head>
  <body>
    <div class="toolbar">
      <table>
        <tr>
          <td><a href="home.php" class="menu">Home</a></td>
          <td><a href="upload.php" class="menu">Upload</a></td>
          <td><img id="logo" src="images/logo_barbagi.png" alt="Gambar ini merupakan logo barbagi"></td>
          <td><a href="myprofile.php" class="menu">My Profile</a></td>
          <td><a href="logout.php" class="menu">Log Out</a></td>
        </tr>
      </table>
    </div>
    <?php
      if ($result=$statement->fetch())
      {
    ?>
      <table>
        <tbody>
          <tr>
            <td>
    <?php
        echo "<img id='profpic' src='" .$result['profile_picture']."' alt='user profile'></td>";
        echo "<td><p id='nama'>".$result['nama']."</p>";
        echo "<p id='desc'>".$result['description']."</p>";
        $MAIN=$result['main_gallery'];
        $query="SELECT * FROM follow WHERE username='$USERNAME' AND user_followed='$PROFILE'";
        $statement=$conn->query($query);
        if ($result=$statement->fetch())
        {
          echo "<form method='post' action='profile.php?profile=".$PROFILE."'>";
          echo "<input class='button' type='submit' value='unfollow' name='unfollow'>";
          echo "</form>";
        }
        else
        {
          echo "<form method='post' action='profile.php?profile=".$PROFILE."'>";
          echo "<input class='button' type='submit' value='follow' name='follow'>";
          echo "</form>";
        }
        echo "</td></tr></tbody></table>";
        $query="SELECT * FROM photo WHERE id_gallery='$MAIN' ORDER BY time_upload DESC LIMIT 1";
        $statement=$conn->query($query);
        if ($result=$statement->fetch())
        {
          echo "<a href='gallery.php?gallery=".$MAIN."'><img src='upload/".$result['id_photo'].".".$result['file_type']."' alt='Main Gallery'></a>";
        }
        else
        {
          echo "Main Gallery has no picture.";
        }
        $query="SELECT * FROM gallery WHERE id_gallery<>'$MAIN' AND username='$PROFILE' AND isprivate=0 ORDER BY dateUpdated";
        $statement=$conn->query($query);
        while ($row=$statement->fetch())
        {
          $GALLERY=$row['id_gallery'];
          $query2="SELECT * FROM photo WHERE id_gallery='$GALLERY' ORDER BY time_upload DESC LIMIT 1";
          $statement2=$conn->query($query2);
          if ($result=$statement2->fetch())
          {
            echo "<a href='gallery.php?gallery=".$GALLERY."'><img src='upload/".$result['id_photo'].".".$result['file_type']."' alt='".$row['nama_gallery']."'></a>";
          }
          else {
            echo $row['nama_gallery']." has no picture.";
          }
        }
      }
      else
      {
        echo "the user you're searching for doesn't exist";
      }
    ?>
  </body>
</html>
