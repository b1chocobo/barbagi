<?php
  session_start();
  if (!isset($_SESSION['user']))
  {
    header('Location: index.php');
  }
  if (!isset($_POST['imgsubmit']))
  {
    header('Location: upload.php');
  }
  $USERNAME=$_SESSION['user'];
  $GALLERY=$_POST['gallery'];
  include "db_connection.php";
  $query="SELECT * from keystore";
  $statement=$conn->query($query);
  $statement->setFetchMode(PDO::FETCH_ASSOC);
  $result=$statement->fetch();
  $next_photo_id=$result['next_photo_id'];
  if (is_uploaded_file($_FILES['imgfile']['tmp_name']))
  {
    $ext=pathinfo($_FILES['imgfile']['name'],PATHINFO_EXTENSION);
    $TITLE=$_POST['imgtitle'];
    $CAPTION=$_POST['imgcap'];
    if (move_uploaded_file($_FILES['imgfile']['tmp_name'],"upload/$next_photo_id".".".$ext))
    {
      $query="INSERT INTO photo (id_gallery,id_photo,file_type,title,caption) values ($GALLERY,'$next_photo_id','$ext','$TITLE','$CAPTION')";
      $statement=$conn->prepare($query);
      $statement->execute();
      $next_photo_id=$next_photo_id+1;
      $query="UPDATE keystore SET next_photo_id='$next_photo_id'";
      $statement=$conn->prepare($query);
      $statement->execute();
      $query="UPDATE gallery SET dateUpdated=CURRENT_TIMESTAMP() WHERE id_gallery=$GALLERY";
      $statement=$conn->prepare($query);
      $statement->execute();
      $_SESSION['success']='success';
      header('Location: upload.php');
    }
  }
?>
