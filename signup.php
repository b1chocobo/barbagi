<?php
  session_start();
  if (isset($_SESSION['user']))
  {
    header('Location: home.php');
  }
?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <title>Barbagi:Sign Up</title>
    <link rel="stylesheet" type="text/css" href="styles/signup.css">
    <script type="text/javascript" src="scripts/signup.js"></script>
  </head>
  <body>
      <br><img src="images/logo_barbagi.png" id="logoBarbagi" alt="Gambar ini merupakan logo barbagi"><br>
      <h1 id="tulisanSignUp">Create Your Account</h1>
      <form name="SIGNUP" method="post" action="signup_auth.php">
        <div class='signUpBox'>
          <input id="inpUsernameSignU" type="text" name="username" placeholder="Username" class="form-style"/>
          <input id="inpPasswordSignUp" type="password" name="password" placeholder="Password" class="form-style"/>
          <input id="inpPasswordSignUp2" type="password" placeholder="Confirm Password" class="form-style" onkeyup="checkPass()"/>
          <input id="inpNamaSignUp" type="text" name="nama" placeholder="Profile Name" class="form-style"/>
          <input id="btnSignUp" class="button" type="submit" value="Sign Up"/>
          <span id="confirmMessage" class="confirmMessage"></span>
        </div>
      </form>
    <?php
      if(isset($_SESSION['empty']))
    	 {
      	 unset($_SESSION['empty']);
      	 echo "<script>alert('Please enter your username, password, and name')</script>";
      }
      if(isset($_SESSION['exist']))
      {
      	 unset($_SESSION['exist']);
      	 echo"<script>alert('Someone already use the username. Please choose a different username')</script>";
      }
      ?>
  </body>
</html>
